package com.pro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaRest1Application {

	public static void main(String[] args) {
		SpringApplication.run(PruebaRest1Application.class, args);
	}

}
