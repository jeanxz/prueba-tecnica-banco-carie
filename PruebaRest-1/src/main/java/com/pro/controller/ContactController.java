package com.pro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pro.entity.Contact;
import com.pro.service.IContactService;

@RestController
public class ContactController {
	
	
	@Autowired
	private IContactService conService;
	
//	POST /create
	@PostMapping("/create")
	public void create(@RequestBody Contact contact) {
		conService.create(contact);
	}
	
//	PUT /update/{id}
	@PutMapping("/update/{id}")
	public void update(@PathVariable Integer id,@RequestBody Contact contact) {
		conService.update(id,contact);
	}
	
//	GET /fetch/{id}
	@GetMapping("/fetch/{id}")
	public Contact fetch(@PathVariable Integer id) {
		return conService.fetch(id);
	}
	
//	DELETE /remove/{id}
	@DeleteMapping("/remove/{id}")
	public void remove(@PathVariable Integer id) {
		conService.remove(id);
	}
	
	@GetMapping("/listing")
	public List<Contact> listing(){
		
		return conService.listing();
		
	}

}
