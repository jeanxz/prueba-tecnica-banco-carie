package com.pro.service;

import java.util.List;

import com.pro.entity.Contact;

public interface IContactService {
	
//	POST /create
//	PUT /update/{id}
//	GET /fetch/{id}
//	DELETE /remove/{id}
	
	public void create(Contact contact);
	public void update(Integer id,Contact contact);
	public Contact fetch (Integer id);
	public void remove(Integer id);
	public List<Contact> listing();

}
