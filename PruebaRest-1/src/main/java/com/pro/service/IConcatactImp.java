package com.pro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pro.dao.ContactDao;
import com.pro.entity.Contact;

@Service
public class IConcatactImp implements IContactService {
	
	@Autowired
	private ContactDao conDao;

	@Override
	@Transactional
	public void create(Contact contact) {
		// TODO Auto-generated method stub
		conDao.save(contact);

	}

	@Override
	@Transactional
	public void update(Integer id,Contact contact) {
		// TODO Auto-generated method stub	
		Contact c = conDao.findById(id).get();
		c.setFullname(contact.getFullname());
		conDao.save(c);
	}

	@Override
	@Transactional
	public Contact fetch(Integer id) {
		// TODO Auto-generated method stub
		return conDao.findById(id).get();
	}

	@Override
	@Transactional
	public void remove(Integer id) {
		// TODO Auto-generated method stub
		Contact contact = conDao.findById(id).get();
		conDao.delete(contact);

	}
	
	@Override
	@Transactional
	public List<Contact> listing(){		
		return (List<Contact>) conDao.findAll();
	}

}
